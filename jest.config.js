module.exports = {
  collectCoverageFrom: [
    '**/*.{js,jsx}',
    '!**/*test.{js,jsx}',
    '!**/test/**',
    '!**/node_modules/**',
    '!**/config/**',
    '!**/coverage/**',
    '!**/dist/**',
  ],
  coverageDirectory: '<rootDir>/coverage',
  projects: [
    {
      rootDir: '<rootDir>/packages/server',
      displayName: 'server',
      testEnvironment: 'jest-environment-db',
      testRegex: '/test/.*_test.js$',
      setupFilesAfterEnv: ['<rootDir>/test/helpers/jest-setup.js'],
    },
    {
      rootDir: '<rootDir>/packages/client',
      displayName: 'client',
      testRegex: '/test/.+test.jsx?$',
      setupFilesAfterEnv: ['<rootDir>/test/jest-setup.js'],
      globals: {
        window: {},
      },
    },
    {
      rootDir: '<rootDir>/packages/db-manager',
      displayName: 'db-manager',
      testMatch: ['**/test/**/*.test.js'],
      setupFilesAfterEnv: ['<rootDir>/test/jest-setup.js'],
      testEnvironment: 'jest-environment-db',
    },
    {
      rootDir: '<rootDir>/components/client',
      displayName: 'components',
      moduleNameMapper: {
        '\\.s?css$': 'identity-obj-proxy',
      },
      transformIgnorePatterns: ['/node_modules/(?!@?pubsweet)'],
      testPathIgnorePatterns: [
        '/node_modules',
        'config/test',
        'model-*',
        '-server',
      ],
      globals: {
        fetch: true,
      },
      setupFilesAfterEnv: ['<rootDir>/test/jest-setup.js'],
    },
    {
      rootDir: '<rootDir>/packages/cli',
      displayName: 'cli',
      testMatch: ['<rootDir>/test/*.test.js', '<rootDir>/test/cli/*.test.js'],
      testPathIgnorePatterns: ['<rootDir>/build/'],
      modulePaths: ['<rootDir>/node_modules'],
      testEnvironment: 'jest-environment-db',
      unmockedModulePathPatterns: ['/src/models'],
      setupFilesAfterEnv: ['<rootDir>/test/helpers/jest-setup.js'],
      verbose: true,
    },
    {
      rootDir: '<rootDir>/packages/logger',
      displayName: 'logger',
      testMatch: ['**/test/**/*test.js'],
      modulePaths: ['<rootDir>/node_modules'],
      testEnvironment: 'node',
      verbose: true,
      setupFilesAfterEnv: ['<rootDir>/test/jest-setup.js'],
    },
    {
      rootDir: '<rootDir>/packages/ui',
      testPathIgnorePatterns: ['/dist'],
      displayName: 'ui',
      moduleNameMapper: {
        '\\.s?css$': 'identity-obj-proxy',
      },
      setupFilesAfterEnv: ['<rootDir>/test/setup/enzyme.js'],
      snapshotSerializers: ['enzyme-to-json/serializer'],
    },
    {
      rootDir: '<rootDir>/packages/base-model',
      displayName: 'base-model',
      testRegex: '/test/.*_test.js$',
      setupFilesAfterEnv: ['<rootDir>/test/jest-setup.js'],
      testEnvironment: 'jest-environment-db',
    },
    {
      rootDir: '<rootDir>/packages/sse',
      displayName: 'sse',
      testRegex: '/test/.*_test.js$',
      testEnvironment: 'node',
    },
    {
      rootDir: '<rootDir>/components/server/model-team',
      displayName: 'model-team',
      testRegex: '/test/.*_test.js$',
      setupFilesAfterEnv: ['<rootDir>/test/jest-setup.js'],
      testEnvironment: 'jest-environment-db',
    },
    {
      rootDir: '<rootDir>/components/server/model-user',
      displayName: 'model-user',
      testRegex: '/test/.*_test.js$',
      setupFilesAfterEnv: ['<rootDir>/test/jest-setup.js'],
      testEnvironment: 'jest-environment-db',
    },
    {
      rootDir: '<rootDir>/components/server/model-fragment',
      displayName: 'model-fragment',
      testRegex: '/test/.*_test.js$',
      setupFilesAfterEnv: ['<rootDir>/test/jest-setup.js'],
      testEnvironment: 'jest-environment-db',
    },
    {
      rootDir: '<rootDir>/components/server/component-password-reset-server',
      displayName: 'component-password-reset-server',
      testRegex: '/test/.*_test.js$',
      setupFilesAfterEnv: ['<rootDir>/test/jest-setup.js'],
      testEnvironment: 'jest-environment-db',
    },
    {
      rootDir: '<rootDir>/components/server/job-xsweet',
      displayName: 'job-xsweet',
      testRegex: '/test/.*_test.js$',
      setupFilesAfterEnv: ['<rootDir>/test/jest-setup.js'],
      testEnvironment: 'node',
    },
    {
      rootDir: '<rootDir>/components/server/component-email-templating',
      displayName: 'component-email-templating',
      testRegex: '/test/.*.test.js$',
      setupFilesAfterEnv: ['<rootDir>/test/jest-setup.js'],
      testEnvironment: 'node',
    },
    {
      rootDir: '<rootDir>/components/server/component-aws-s3',
      displayName: 'component-aws-s3',
      testRegex: '/test/.*.test.js$',
      setupFilesAfterEnv: ['<rootDir>/test/jest-setup.js'],
      testEnvironment: 'node',
    },
  ],
}
