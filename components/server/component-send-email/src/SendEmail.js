const logger = require('@pubsweet/logger')
const nodemailer = require('nodemailer')
const config = require('config')

let mailerConfig

if (config.has('mailer.path')) {
  mailerConfig = require(config.get('mailer.path'))
} else if (config.has('mailer.transport')) {
  mailerConfig = config.get('mailer')
}

const docsUrl =
  'https://gitlab.coko.foundation/pubsweet/pubsweet/tree/master/packages/components/server/component-send-email'

module.exports = {
  send: mailData => {
    if (!mailerConfig || !mailerConfig.transport) {
      throw new Error(
        `Mailer: The configuration is either invalid or missing. Check here: ${docsUrl}`,
      )
    }

    const transporter = nodemailer.createTransport(mailerConfig.transport)

    return transporter
      .sendMail(mailData)
      .then(info => {
        if (process.env.NODE_ENV === 'development') {
          try {
            logger.info(
              `Email sent. Preview available at: ${nodemailer.getTestMessageUrl(
                info,
              )}`,
            )
          } catch (err) {
            logger.info(`Email sent.`)
          }
        }
        return info
      })
      .catch(err => {
        logger.error(`Failed to send email ${err}`)
      })
  },
}
